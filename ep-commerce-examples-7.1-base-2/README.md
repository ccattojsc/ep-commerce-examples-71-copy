This repository contains working example code and exercises. Its intended purpose is to facilitate both self-learning and instructor led training. 

Also this repository can be used as reference material for developers working on client projects.

### Repository structure
The `master` branch is an exact copy of the `extensions` project for V7.1.x.

The `base` branch contains common code that will be harvested back into the product in the next major release. This common code is inherited by all of the tutorial branches. 

Tutorial branches can have the following structure and contain:
* `example` - reference code only
* `example+exercise` - reference code and an exercise based on the reference
* `exercise` - an exercise for the student to complete
* `answer` - a solution that can be used to verify the associated exercise

### List of available tutorials
Click this link to view all tutorial branches in the GitLab repository: [https://code.elasticpath.com/education/ep-commerce-examples-7.1/branches](https://code.elasticpath.com/education/ep-commerce-examples-7.1/branches).

### Tutorial structure
Within the `exercise` or `example+exercise` branch of the chosen tutorial, there will be a `Tutorial.md` file with the following contents:
 * Overview / Outline
 * Prerequisite Knowledge 
 * Business Requirements
 * (Reference example - if included)
 * Coding Exercise

### Comparing between branches 
Use this [GitLab link](https://code.elasticpath.com/education/ep-commerce-examples-7.1/compare?from=base&to=base) or click the “Compare” button in the GitLab tutorial branch to get the diff between branches. Compare between the base branch and a tutorial branch to view the changes.

To view a tutorial solution, the “From” option should be the tutorial `exercise` or `example+exercise` branch, and the “To” option should be the tutorial `answer` branch.

### Developer environment setup
If you are not using a VM set up by Elastic Path as part of an instructor lead training session, please follow [these instructions](https://developers.elasticpath.com/commerce/7.1/Starting-Construction/Getting-Started/Construction-Introduction) to set up your developer environment. 
